import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'kalkulator-ocen', pathMatch: 'full'},
    {path: 'kalkulator-ocen', loadChildren: './feature/points-calculator/points-calculator.module#PointsCalculatorModule'},
    {path: 'srednia-wazona', loadChildren: './feature/weighted-average/weighted-average.module#WeightedAverageModule'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
