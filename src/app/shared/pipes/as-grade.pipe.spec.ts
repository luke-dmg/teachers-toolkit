import {AsGradePipe} from './as-grade.pipe';
import {
    ExtendedGradingSystem,
    ExtendedSignedGradingSystem,
    GradingSystem,
    StandardGradingSystem,
    StandardSignedGradingSystem
} from '../models/grading-system';

describe('AsGrade', () => {

    const pipe = new AsGradePipe();

    const gradingSystemsWithTestValues = [{
        gradingSystem: new StandardGradingSystem(),
        testValuePairs: [
            [2, '2'],
            [2.5, '2'],
            [5.5, '5'],
            [6, '5'],
            [null, 'brak'],
            [0, 'brak'],
            [7, '5'],
        ],
    }, {
        gradingSystem: new StandardSignedGradingSystem(),
        testValuePairs: [
            [2, '2'],
            [2.5, '2+'],
            [5.5, '5'],
            [6, '5'],
            [null, 'brak'],
            [0, 'brak'],
            [7, '5'],
        ],
    }, {
        gradingSystem: new ExtendedGradingSystem(),
        testValuePairs: [
            [2, '2'],
            [2.5, '2'],
            [5.5, '5'],
            [6, '6'],
            [null, 'brak'],
            [0, 'brak'],
            [7, '6'],
        ],
    }, {
        gradingSystem: new ExtendedSignedGradingSystem(),
        testValuePairs: [
            [2, '2'],
            [2.5, '2+'],
            [5.5, '5+'],
            [6, '6'],
            [null, 'brak'],
            [0, 'brak'],
            [7, '6'],
        ],
    }];

    it('should create an instance', () => {
        expect(pipe).toBeDefined();
    });

    it('should return brak for wrong grading system', () => {
        expect(pipe.transform(5, new GradingSystem())).toBe('brak');
    });

    it('should return brak given null grading system', () => {
        expect(pipe.transform(5, null)).toBe('brak');
    });

    for (const testItem of gradingSystemsWithTestValues) {
        describe(testItem.gradingSystem.constructor.name + ' valid values', () => {
            for (const testValuePair of testItem.testValuePairs) {
                it('should return ' + testValuePair[1] + ' given ' + testValuePair[0], () => {
                    expect(pipe.transform(testValuePair[0] as number, testItem.gradingSystem)).toBe(testValuePair[1] as string);
                });
            }
        });
    }
});
