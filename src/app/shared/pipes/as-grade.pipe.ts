import {Pipe, PipeTransform} from '@angular/core';
import {GradingSystem} from '../models/grading-system';

/*
 * Converts floating point number to grade based on GRADES array.
 * Takes GradingSystem as argument.
 *
 * Usage:
 *   value | asGrade: currentGradingSystem
 *
 * Example 1:
 *   {{ 1.75 | asGrade: currentGradingSystem }}
 *   formats to: 2-
 *
 * Example 2:
 *   {{ 4.5 | asGrade: currentGradingSystem }}
 *   formats to: 4+
*/
@Pipe({name: 'asGrade'})
export class AsGradePipe implements PipeTransform {
    transform(value: number, gradingSystem: GradingSystem): string {
        try {
            return gradingSystem.gradeFromValue(value).name;
        } catch (e) {
            return 'brak';
        }
    }
}
