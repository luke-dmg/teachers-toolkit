import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    HostBinding,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Self,
    ViewChild
} from '@angular/core';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors, Validator} from '@angular/forms';
import {MatFormFieldControl, MatSelect} from '@angular/material';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {GradingSystem} from '../../models/grading-system';

@Component({
    selector: 'app-grading-system-selector',
    providers: [{
        provide: MatFormFieldControl,
        useExisting: GradingSystemSelectorComponent,
    }],
    templateUrl: './grading-system-selector.component.html',
    styleUrls: ['./grading-system-selector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GradingSystemSelectorComponent
    // implements various interfaces to use in material form field and reactive forms
    implements OnInit, OnDestroy, ControlValueAccessor, Validator, MatFormFieldControl<GradingSystem> {

    static nextId = 0;

    autofilled: boolean;
    controlType = 'grading-system-selector-input';
    errorState = false;
    focused: boolean;
    stateChanges = new Subject<void>();

    @HostBinding() id = `grading-system-selector-input-${GradingSystemSelectorComponent.nextId++}`;
    @HostBinding('attr.aria-describedby') describedBy = '';

    @ViewChild('select') select: MatSelect;

    constructor(
        @Optional() @Self() public ngControl: NgControl,
        private fm: FocusMonitor,
        private elRef: ElementRef<HTMLElement>,
    ) {
        if (this.ngControl != null) {
            // setting the value accessor directly (instead of using the providers)
            // to avoid running into a circular import
            this.ngControl.valueAccessor = this;
        }
        fm.monitor(elRef.nativeElement, true).subscribe(origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        });
    }

    @HostBinding('class.floating')
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }

    private _required = false;

    @Input()
    get required() {
        return this._required;
    }

    set required(req) {
        this._required = coerceBooleanProperty(req);
        this.stateChanges.next();
    }

    private _disabled = false;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: boolean) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _value: GradingSystem | null;

    get value() {
        return this._value;
    }

    set value(value: GradingSystem | null) {
        this._value = value;
        this.stateChanges.next();
    }

    private _gradingSystems: GradingSystem[];

    get gradingSystems(): GradingSystem[] {
        return this._gradingSystems;
    }

    @Input() set gradingSystems(gss: GradingSystem[]) {
        this._gradingSystems = gss;
        // check if array contains any objects
        if (gss && gss.length) {
            // select first object
            this.value = gss[0];
            this.onChange(this.value);
        }
    }

    private _placeholder: string;

    @Input()
    get placeholder() {
        return this._placeholder;
    }

    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }

    get empty() {
        return !this.value;
    }

    onChange = (any) => any;

    onTouched = (any) => any;

    onValidatorChange = (any) => any;

    ngOnInit() {
    }

    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef.nativeElement);
    }

    onValueChange(gradingSystem: GradingSystem) {
        this.value = gradingSystem;
        this.onChange(this.value);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    registerOnValidatorChange(fn: () => void): void {
        this.onValidatorChange = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    writeValue(gradingSystem: any): void {
        this.value = gradingSystem;
    }

    validate(control: AbstractControl): ValidationErrors | null {
        return null;
    }

    onContainerClick(event: MouseEvent) {
        // when clicking the container automatically focus and open selection list
        this.select.focus();
        this.select.open();
    }

    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }
}
