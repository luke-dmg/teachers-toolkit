import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingSystemSelectorComponent } from './grading-system-selector.component';

describe('GradingSystemSelectorComponent', () => {
  let component: GradingSystemSelectorComponent;
  let fixture: ComponentFixture<GradingSystemSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradingSystemSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingSystemSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
