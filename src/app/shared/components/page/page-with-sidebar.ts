import {ChangeDetectorRef, HostListener, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material';
import {Page} from './page';
import {Title} from '@angular/platform-browser';

export class PageWithSidebar extends Page {
    @ViewChild(MatSidenav) sidebar: MatSidenav;

    constructor(
        titleService: Title,
        protected ref: ChangeDetectorRef) {
        super(titleService);
    }

    @HostListener('panright')
    openSidebar() {
        if (this.isMobile) {
            this.toggleSidebar(true);
        }
    }

    @HostListener('panleft')
    closeSidebar() {
        if (this.isMobile) {
            this.toggleSidebar(false);
        }
    }

    toggleSidebar(isOpen?: boolean) {
        this.sidebar.toggle(isOpen).catch((err) => console.error(err));
        this.ref.detectChanges();
    }
}
