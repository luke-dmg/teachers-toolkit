import {OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Subject} from 'rxjs';

export class Page implements OnInit, OnDestroy {

    pageName: string;
    isMobile = new Subject<boolean>();
    isDestroyed = new Subject();

    constructor(
        protected titleService: Title,
    ) {
    }

    ngOnInit() {
        this.titleService.setTitle(this.pageName);
    }

    ngOnDestroy(): void {
        this.isDestroyed.next();
        this.isDestroyed.complete();
        this.isMobile.complete();
    }
}
