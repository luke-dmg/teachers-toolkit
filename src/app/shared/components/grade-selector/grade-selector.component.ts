import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    HostBinding,
    Input,
    OnDestroy,
    OnInit,
    Optional,
    Self,
    ViewChild
} from '@angular/core';
import {AbstractControl, ControlValueAccessor, NgControl, ValidationErrors, Validator} from '@angular/forms';
import {GradeThreshold} from '../../models/grade-threshold';
import {Grade} from '../../models/grade';
import {MatFormFieldControl, MatSelect} from '@angular/material';
import {Subject} from 'rxjs';
import {FocusMonitor} from '@angular/cdk/a11y';
import {coerceBooleanProperty} from '@angular/cdk/coercion';

@Component({
    selector: 'app-grade-selector',
    providers: [{
        provide: MatFormFieldControl,
        useExisting: GradeSelectorComponent,
    }],
    templateUrl: './grade-selector.component.html',
    styleUrls: ['./grade-selector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GradeSelectorComponent
    // implements various interfaces to use in material form field and reactive forms
    implements OnInit, OnDestroy, ControlValueAccessor, Validator, MatFormFieldControl<Grade> {

    static nextId = 0;

    autofilled: boolean;
    controlType = 'grade-selector-input';
    errorState = false;
    focused: boolean;
    stateChanges = new Subject<void>();

    @HostBinding() id = `grade-selector-input-${GradeSelectorComponent.nextId++}`;
    @HostBinding('attr.aria-describedby') describedBy = '';

    @ViewChild('select') select: MatSelect;
    onChange: (arg: any) => any;
    onTouched: (arg: any) => any;
    onValidatorChange: (arg: any) => any;

    constructor(
        @Optional() @Self() public ngControl: NgControl,
        private fm: FocusMonitor,
        private elRef: ElementRef<HTMLElement>,
    ) {
        if (this.ngControl != null) {
            // Setting the value accessor directly (instead of using
            // the providers) to avoid running into a circular import.
            this.ngControl.valueAccessor = this;
        }
        fm.monitor(elRef.nativeElement, true).subscribe(origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        });
    }

    @HostBinding('class.floating')
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }

    private _required = false;

    @Input()
    get required() {
        return this._required;
    }

    set required(req) {
        this._required = coerceBooleanProperty(req);
        this.stateChanges.next();
    }

    private _disabled = false;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(value: boolean) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }

    private _value: Grade | null;

    get value() {
        return this._value;
    }

    set value(value: Grade | null) {
        this._value = value;
        this.stateChanges.next();
    }

    private _gradeThresholds: GradeThreshold[];

    get gradeThresholds(): GradeThreshold[] {
        return this._gradeThresholds;
    }

    @Input() set gradeThresholds(gts: GradeThreshold[]) {
        this._gradeThresholds = gts;
        // check if array contains any objects
        if (gts && gts.length) {
            // select first object
            this.value = gts[0].grade;
            this.onChange(this.value);
        }
    }

    private _placeholder: string;

    @Input()
    get placeholder() {
        return this._placeholder;
    }

    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }

    get empty() {
        return !this.value;
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef.nativeElement);
    }

    onValueChange(grade: Grade) {
        this.value = grade;
        this.onChange(this.value);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    registerOnValidatorChange(fn: () => void): void {
        this.onValidatorChange = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    writeValue(grade: any): void {
        this.value = grade;
    }

    validate(control: AbstractControl): ValidationErrors | null {
        return null;
    }

    onContainerClick(event: MouseEvent) {
        // when clicking the container automatically focus and open selection list
        this.select.focus();
        this.select.open();
    }

    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }
}
