import {NgModule} from '@angular/core';
import {AsGradePipe} from './pipes/as-grade.pipe';
import {GradeSelectorComponent} from './components/grade-selector/grade-selector.component';
import {GradingSystemSelectorComponent} from './components/grading-system-selector/grading-system-selector.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SharedLayoutModule} from './shared-layout.module';

@NgModule({
    declarations: [
        AsGradePipe,
        GradeSelectorComponent,
        GradingSystemSelectorComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedLayoutModule,
    ],
    exports: [
        CommonModule,
        SharedLayoutModule,
        ReactiveFormsModule,
        AsGradePipe,
        GradeSelectorComponent,
        GradingSystemSelectorComponent,
    ],
})
export class SharedModule {
}
