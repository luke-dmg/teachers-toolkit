import {Grade} from './grade';

export interface PointsThreshold {
    grade: Grade;
    min: number;
    max: number;
}
