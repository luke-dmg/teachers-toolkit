import {Grade} from './grade';

export class WeightedGrade {
    grade: Grade;
    weight: number;
}
