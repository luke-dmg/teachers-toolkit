export interface NavItem {
    label: string;
    path: string;
    icon: string;
    class?: string;
}
