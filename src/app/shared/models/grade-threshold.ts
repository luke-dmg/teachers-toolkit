import {Grade} from './grade';

export class GradeThreshold {
    grade: Grade;
    requiredPercentage: number;
}
