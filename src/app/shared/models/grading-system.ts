import {GradeThreshold} from './grade-threshold';

export class GradingSystem {
    // thresholds should be sorted from highest (threshold[0]) to lowest (threshold[threshold.length - 1])
    readonly thresholds: GradeThreshold[];
    readonly name: string;

    maxGrade() {
        return this.thresholds.length ? this.thresholds[0].grade : null;
    }

    gradeFromPercentage(percentage: number) {
        for (const threshold of this.thresholds) {
            if (percentage >= threshold.requiredPercentage) {
                return threshold.grade;
            }
        }
        return null;
    }

    gradeFromValue(value: number) {
        for (const threshold of this.thresholds) {
            if (threshold.grade.value <= value && value) {
                return threshold.grade;
            }
        }
        return null;
    }
}

// @todo convert to factory factory

export class StandardGradingSystem extends GradingSystem {
    readonly name = '1-5';
    readonly thresholds = [
        {grade: {name: '5', value: 5}, requiredPercentage: 90},
        {grade: {name: '4', value: 4}, requiredPercentage: 75},
        {grade: {name: '3', value: 3}, requiredPercentage: 55},
        {grade: {name: '2', value: 2}, requiredPercentage: 40},
        {grade: {name: '1', value: 1}, requiredPercentage: 0},
    ];
}

export class ExtendedGradingSystem extends GradingSystem {
    readonly name = '1-6';
    readonly thresholds = [
        {grade: {name: '6', value: 6}, requiredPercentage: 98},
        {grade: {name: '5', value: 5}, requiredPercentage: 90},
        {grade: {name: '4', value: 4}, requiredPercentage: 75},
        {grade: {name: '3', value: 3}, requiredPercentage: 55},
        {grade: {name: '2', value: 2}, requiredPercentage: 40},
        {grade: {name: '1', value: 1}, requiredPercentage: 0},
    ];
}

export class StandardSignedGradingSystem extends GradingSystem {
    readonly name = '1-5 +/-';
    readonly thresholds = [
        {grade: {name: '5', value: 5}, requiredPercentage: 90},
        {grade: {name: '5-', value: 4.75}, requiredPercentage: 87},
        {grade: {name: '4+', value: 4.5}, requiredPercentage: 83},
        {grade: {name: '4', value: 4}, requiredPercentage: 75},
        {grade: {name: '4-', value: 3.75}, requiredPercentage: 70},
        {grade: {name: '3+', value: 3.5}, requiredPercentage: 65},
        {grade: {name: '3', value: 3}, requiredPercentage: 55},
        {grade: {name: '3-', value: 2.75}, requiredPercentage: 52},
        {grade: {name: '2+', value: 2.5}, requiredPercentage: 48},
        {grade: {name: '2', value: 2}, requiredPercentage: 40},
        {grade: {name: '2-', value: 1.75}, requiredPercentage: 38},
        {grade: {name: '1+', value: 1.5}, requiredPercentage: 35},
        {grade: {name: '1', value: 1}, requiredPercentage: 0},
    ];
}

export class ExtendedSignedGradingSystem extends GradingSystem {
    readonly name = '1-6 +/-';
    readonly thresholds = [
        {grade: {name: '6', value: 6}, requiredPercentage: 98},
        {grade: {name: '6-', value: 5.75}, requiredPercentage: 96},
        {grade: {name: '5+', value: 5.5}, requiredPercentage: 94},
        {grade: {name: '5', value: 5}, requiredPercentage: 90},
        {grade: {name: '5-', value: 4.75}, requiredPercentage: 87},
        {grade: {name: '4+', value: 4.5}, requiredPercentage: 83},
        {grade: {name: '4', value: 4}, requiredPercentage: 75},
        {grade: {name: '4-', value: 3.75}, requiredPercentage: 70},
        {grade: {name: '3+', value: 3.5}, requiredPercentage: 65},
        {grade: {name: '3', value: 3}, requiredPercentage: 55},
        {grade: {name: '3-', value: 2.75}, requiredPercentage: 52},
        {grade: {name: '2+', value: 2.5}, requiredPercentage: 48},
        {grade: {name: '2', value: 2}, requiredPercentage: 40},
        {grade: {name: '2-', value: 1.75}, requiredPercentage: 38},
        {grade: {name: '1+', value: 1.5}, requiredPercentage: 35},
        {grade: {name: '1', value: 1}, requiredPercentage: 0},
    ];
}
