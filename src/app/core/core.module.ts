import {NgModule, Optional, SkipSelf} from '@angular/core';
import {NavbarComponent} from './components/navbar/navbar.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../../environments/environment';
import {SharedLayoutModule} from '../shared/shared-layout.module';
import {AppRoutingModule} from '../app-routing.module';
import {UpdateComponent} from './components/update/update.component';

@NgModule({
    declarations: [
        NavbarComponent,
        UpdateComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        SharedLayoutModule,
    ],
    exports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ServiceWorkerModule,
        SharedLayoutModule,
        NavbarComponent,
        UpdateComponent,
    ],
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() core: CoreModule) {
        if (core) {
            throw new Error('You should import core module only in the root module');
        }
    }
}
