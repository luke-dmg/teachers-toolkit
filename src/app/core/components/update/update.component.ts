import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';
import {take, takeWhile} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';

@Component({
    selector: 'app-update',
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateComponent implements OnInit {

    constructor(
        private swUpdate: SwUpdate,
        private snackBar: MatSnackBar,
    ) {
    }

    ngOnInit() {
        // listen for updates and display prompt to reload if found
        if (this.swUpdate.isEnabled) {
            this.swUpdate.available
                .pipe(
                    takeWhile(() => this.swUpdate.isEnabled)
                )
                .subscribe(() => {
                    this.snackBar
                        .open('Dostępna jest nowa wersja aplikacji.', 'Aktualizuj', {duration: 10000})
                        .onAction()
                        .pipe(
                            take(1)
                        )
                        .subscribe(() => window.location.reload());
                });
        }
    }
}
