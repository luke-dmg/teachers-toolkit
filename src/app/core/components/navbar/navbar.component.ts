import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavItem} from '../../../shared/models/nav-item';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent implements OnInit {

    @Input() isSidebarToggleVisible: boolean;
    @Input() navItems: NavItem[];
    @Input() currentAppTitle: string;
    @Output() toggleSidebar: EventEmitter<null> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {

    }

    emitToggleSidebar() {
        this.toggleSidebar.emit();
    }
}
