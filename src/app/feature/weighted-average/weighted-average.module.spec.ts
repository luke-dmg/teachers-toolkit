import { WeightedAverageModule } from './weighted-average.module';

describe('WeightedAverageModule', () => {
  let weightedAverageModule: WeightedAverageModule;

  beforeEach(() => {
    weightedAverageModule = new WeightedAverageModule();
  });

  it('should create an instance', () => {
    expect(weightedAverageModule).toBeTruthy();
  });
});
