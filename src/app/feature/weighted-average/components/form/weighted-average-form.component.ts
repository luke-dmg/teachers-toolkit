import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GradingSystem} from '../../../../shared/models/grading-system';
import {WeightedGrade} from '../../../../shared/models/weighted-grade';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {filter, map, startWith, tap} from 'rxjs/operators';
import {GradeThreshold} from '../../../../shared/models/grade-threshold';

@Component({
    selector: 'app-weighted-average-form',
    templateUrl: './weighted-average-form.component.html',
    styleUrls: ['./weighted-average-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeightedAverageFormComponent implements OnInit {

    @Input() minGradeWeight = 1;
    @Input() maxGradeWeight = 10;
    @Input() gradingSystems: GradingSystem[];

    @Output() addWeightedGrade = new EventEmitter<WeightedGrade>();
    @Output() setGradingSystem = new EventEmitter<GradingSystem>();

    waForm: FormGroup;
    selectedGradeThresholds$: Observable<GradeThreshold>;

    constructor(private fb: FormBuilder) {
    }

    get waFormGradingSystem() {
        return this.waForm.get('gradingSystem');
    }

    get waFormGrade() {
        return this.waForm.get('grade');
    }

    get waFormWeight() {
        return this.waForm.get('weight');
    }

    ngOnInit(): void {
        this.waForm = this.fb.group({
            gradingSystem: [
                '', [
                    Validators.required,
                ]],
            grade: [
                '', [
                    Validators.required,
                ]],
            weight: [
                this.minGradeWeight, [
                    Validators.required,
                    Validators.min(this.minGradeWeight),
                    Validators.max(this.maxGradeWeight),
                ]],
        });
        // observe grading system changes
        this.selectedGradeThresholds$ = this.waFormGradingSystem.valueChanges.pipe(
            // get initial value from variable (waForm is instantiated after @inputs)
            startWith((this.gradingSystems && this.gradingSystems.length) ? this.gradingSystems[0] : null),
            // push if it's not null
            filter(value => value !== null),
            // emit changed grading system
            tap(gradingSystem => this.setGradingSystem.emit(gradingSystem)),
            // convert to grade thresholds
            map(gradingSystem => gradingSystem.thresholds),
        );
    }

    _addWeightedGrade() {
        this.addWeightedGrade.emit({
            grade: this.waFormGrade.value,
            weight: this.waFormWeight.value,
        });
    }
}
