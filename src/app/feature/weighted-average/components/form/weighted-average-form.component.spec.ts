import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeightedAverageFormComponent} from './weighted-average-form.component';

describe('WeightedAverageFormComponent', () => {
    let component: WeightedAverageFormComponent;
    let fixture: ComponentFixture<WeightedAverageFormComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WeightedAverageFormComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WeightedAverageFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
