import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeightedAverageTableComponent} from './weighted-average-table.component';

describe('WeightedAverageTableComponent', () => {
    let component: WeightedAverageTableComponent;
    let fixture: ComponentFixture<WeightedAverageTableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WeightedAverageTableComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WeightedAverageTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
