import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatSort, MatTable, MatTableDataSource} from '@angular/material';
import {WeightedGrade} from '../../../../shared/models/weighted-grade';
import {GradingSystem} from '../../../../shared/models/grading-system';

@Component({
    selector: 'app-weighted-average-table',
    templateUrl: './weighted-average-table.component.html',
    styleUrls: ['./weighted-average-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeightedAverageTableComponent implements OnInit {

    @ViewChild(MatTable) table: MatTable<any>;
    @ViewChild(MatSort) sort: MatSort;

    @Input() currentGradingSystem: GradingSystem;

    @Output() removeWeightedGrade = new EventEmitter<WeightedGrade>();
    @Output() resetData = new EventEmitter();

    tableData = new MatTableDataSource<WeightedGrade>();
    tableColumns = ['grade', 'weight', 'action'];
    weightedAverage: number;

    constructor() {

    }

    get weightedGrades(): WeightedGrade[] {
        return this.tableData.data;
    }

    @Input()
    set weightedGrades(weightedGrades: WeightedGrade[]) {
        this.tableData.data = weightedGrades;
        this.calculateAverage();
    }

    ngOnInit() {
        this.tableData.sortingDataAccessor = (item, property) => {
            switch (property) {
                case 'grade':
                    return item[property].value;
                default:
                    return item[property];
            }
        };
        this.tableData.sort = this.sort;
    }

    remove(weightedGrade: WeightedGrade) {
        this.removeWeightedGrade.emit(weightedGrade);
    }

    calculateAverage() {
        let totalGrades = 0;
        let totalWeights = 0;
        this.tableData.data.forEach((weightedGrade) => {
            totalGrades += weightedGrade.grade.value * weightedGrade.weight;
            totalWeights += weightedGrade.weight;
        });
        this.weightedAverage = (totalWeights === 0) ? 0 : totalGrades / totalWeights;
    }

    reset() {
        this.resetData.emit();
    }
}
