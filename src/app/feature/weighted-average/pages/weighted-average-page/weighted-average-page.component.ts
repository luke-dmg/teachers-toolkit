import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {PageWithSidebar} from '../../../../shared/components/page/page-with-sidebar';
import {
    ExtendedGradingSystem,
    ExtendedSignedGradingSystem,
    GradingSystem,
    StandardGradingSystem,
    StandardSignedGradingSystem
} from '../../../../shared/models/grading-system';
import {WeightedGrade} from '../../../../shared/models/weighted-grade';
import {BehaviorSubject} from 'rxjs';

@Component({
    selector: 'app-weighted-average-page',
    templateUrl: './weighted-average-page.component.html',
    styleUrls: ['./weighted-average-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeightedAveragePageComponent extends PageWithSidebar {
    gradingSystems = [
        new StandardGradingSystem(),
        new ExtendedGradingSystem(),
        new StandardSignedGradingSystem(),
        new ExtendedSignedGradingSystem(),
    ];
    currentGradingSystem = new BehaviorSubject<GradingSystem>(null);
    weightedGrades = new BehaviorSubject<WeightedGrade[]>([]);

    constructor(titleService: Title, ref: ChangeDetectorRef) {
        super(titleService, ref);
        this.pageName = 'Średnia ważona';
    }

    addWeightedGrade(weightedGrade: WeightedGrade) {
        this.weightedGrades.next([...this.weightedGrades.getValue(), weightedGrade]);
    }

    removeWeightedGrade(weightedGrade: WeightedGrade) {
        this.weightedGrades.next(this.weightedGrades.getValue().filter((element) => element !== weightedGrade));
    }

    resetData() {
        this.weightedGrades.next([]);
    }

    setGradingSystem(gradingSystem: GradingSystem) {
        this.currentGradingSystem.next(gradingSystem);
    }
}
