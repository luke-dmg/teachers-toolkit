import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeightedAveragePageComponent } from './weighted-average-page.component';

describe('WeightedAveragePageComponent', () => {
  let component: WeightedAveragePageComponent;
  let fixture: ComponentFixture<WeightedAveragePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeightedAveragePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeightedAveragePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
