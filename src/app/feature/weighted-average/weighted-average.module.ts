import {NgModule} from '@angular/core';
import {WeightedAverageRoutingModule} from './weighted-average-routing.module';
import {WeightedAveragePageComponent} from './pages/weighted-average-page/weighted-average-page.component';
import {WeightedAverageFormComponent} from './components/form/weighted-average-form.component';
import {WeightedAverageTableComponent} from './components/table/weighted-average-table.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
        WeightedAverageRoutingModule,
    ],
    declarations: [
        WeightedAveragePageComponent,
        WeightedAverageFormComponent,
        WeightedAverageTableComponent,
    ]
})
export class WeightedAverageModule {
}
