import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WeightedAveragePageComponent} from './pages/weighted-average-page/weighted-average-page.component';

const routes: Routes = [
    {path: '', component: WeightedAveragePageComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WeightedAverageRoutingModule {
}
