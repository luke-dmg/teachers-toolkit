import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PointsCalculatorPageComponent} from './pages/points-calculator-page/points-calculator-page.component';

const routes: Routes = [
    {path: '', component: PointsCalculatorPageComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PointsCalculatorRoutingModule {
}
