import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {
    ExtendedGradingSystem,
    ExtendedSignedGradingSystem,
    StandardGradingSystem,
    StandardSignedGradingSystem
} from '../../../../shared/models/grading-system';
import {BehaviorSubject} from 'rxjs';
import {PointsThreshold} from '../../../../shared/models/points-threshold';
import {PageWithSidebar} from '../../../../shared/components/page/page-with-sidebar';

@Component({
    selector: 'app-points-calculator-page',
    templateUrl: './points-calculator-page.component.html',
    styleUrls: ['./points-calculator-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointsCalculatorPageComponent extends PageWithSidebar {

    minPoints = 10;
    maxPoints = 200;
    precisions = [1, 2, 4];
    gradingSystems = [
        new StandardGradingSystem(),
        new ExtendedGradingSystem(),
        new StandardSignedGradingSystem(),
        new ExtendedSignedGradingSystem(),
    ];
    pointsThresholds = new BehaviorSubject<PointsThreshold[]>([]);

    constructor(titleService: Title, ref: ChangeDetectorRef) {
        super(titleService, ref);
        this.pageName = 'Kalkulator ocen';
    }

    updateTableData(thresholds: PointsThreshold[]) {
        this.pointsThresholds.next(thresholds);
    }
}
