import {NgModule} from '@angular/core';
import {PointsCalculatorRoutingModule} from './points-calculator-routing.module';
import {PointsCalculatorPageComponent} from './pages/points-calculator-page/points-calculator-page.component';
import {PointsCalculatorFormComponent} from './components/form/points-calculator-form.component';
import {PointsCalculatorTableComponent} from './components/table/points-calculator-table.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
        PointsCalculatorRoutingModule,
    ],
    declarations: [
        PointsCalculatorTableComponent,
        PointsCalculatorFormComponent,
        PointsCalculatorPageComponent,
    ]
})
export class PointsCalculatorModule {
}
