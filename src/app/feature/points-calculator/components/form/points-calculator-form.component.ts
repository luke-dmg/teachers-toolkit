import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {GradingSystem} from '../../../../shared/models/grading-system';
import {PointsThreshold} from '../../../../shared/models/points-threshold';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-points-calculator-form',
    templateUrl: './points-calculator-form.component.html',
    styleUrls: ['./points-calculator-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointsCalculatorFormComponent implements OnInit, OnDestroy {

    @Input() precisions: number[] = [1, 0.5];
    @Input() minPoints = 10;
    @Input() maxPoints = 100;
    @Input() gradingSystems: GradingSystem[];

    @Output() pointsThresholds = new EventEmitter<PointsThreshold[]>();

    isDestroyed$ = new Subject();
    pcForm: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    get pcFormGradingSystem() {
        return this.pcForm.get('gradingSystem');
    }

    get pcFormPrecision() {
        return this.pcForm.get('precision');
    }

    get pcFormMaxPoints() {
        return this.pcForm.get('maxPoints');
    }

    ngOnInit() {
        this.pcForm = this.fb.group({
            gradingSystem: [
                '', [
                    Validators.required,
                ]],
            precision: [
                this.precisions ? this.precisions[0] : '', [
                    Validators.required,
                ]],
            maxPoints: [
                this.minPoints, [
                    Validators.required,
                    Validators.min(this.minPoints),
                    Validators.max(this.maxPoints),
                ]],
        });
        // emit when form changes
        this.pcForm.valueChanges
            .pipe(
                takeUntil(this.isDestroyed$),
            )
            .subscribe(() => {
                if (this.pcForm.valid) {
                    this._calculatePointsThresholds();
                }
            });
    }

    _calculatePointsThresholds() {
        const currentGradingSystem = this.pcFormGradingSystem.value;
        const currentPrecision = this.pcFormPrecision.value;
        const currentMaxPoints = this.pcFormMaxPoints.value;
        // create empty array
        const pointsThresholds: PointsThreshold[] = [];
        // set initial maximum
        let pointsMax = currentMaxPoints * currentPrecision;
        // loop through every grade
        for (const threshold of currentGradingSystem.thresholds) {
            // calculate minimum
            const pointsMin = Math.ceil(currentMaxPoints * currentPrecision * threshold.requiredPercentage / 100);
            // compare max and min (needed for small max points)
            pointsMax = Math.max(pointsMax, pointsMin);
            // save grade and it's points range
            pointsThresholds.push({
                grade: threshold.grade,
                min: pointsMin / currentPrecision,
                max: pointsMax / currentPrecision,
            });
            // set maximum for next grade
            pointsMax = pointsMin - 1;
        }
        // emit result
        this.pointsThresholds.emit(pointsThresholds);
    }

    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
