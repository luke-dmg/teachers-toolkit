import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PointsCalculatorTableComponent} from './points-calculator-table.component';

describe('PointsCalculatorTableComponent', () => {
    let component: PointsCalculatorTableComponent;
    let fixture: ComponentFixture<PointsCalculatorTableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PointsCalculatorTableComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PointsCalculatorTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
