import {AfterViewInit, ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {MatSort, MatTable, MatTableDataSource} from '@angular/material';
import {PointsThreshold} from '../../../../shared/models/points-threshold';

@Component({
    selector: 'app-points-calculator-table',
    templateUrl: './points-calculator-table.component.html',
    styleUrls: ['./points-calculator-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointsCalculatorTableComponent implements AfterViewInit {

    @ViewChild(MatTable) table: MatTable<any>;
    @ViewChild(MatSort) sort: MatSort;
    tableData = new MatTableDataSource<PointsThreshold>();
    tableColumns = ['grade', 'min', 'max'];

    constructor() {
        this.tableData.sortingDataAccessor = (item, property) => {
            switch (property) {
                case 'grade':
                    return item[property].value;
                default:
                    return item[property];
            }
        };
    }

    @Input() set pointsThresholds(pointsThresholds: PointsThreshold[]) {
        this.tableData.data = pointsThresholds;
    }

    ngAfterViewInit() {
        this.tableData.sort = this.sort;
    }
}
