import {PointsCalculatorModule} from './points-calculator.module';

describe('PointsCalculatorModule', () => {
    let pointsCalculatorModule: PointsCalculatorModule;

    beforeEach(() => {
        pointsCalculatorModule = new PointsCalculatorModule();
    });

    it('should create an instance', () => {
        expect(pointsCalculatorModule).toBeTruthy();
    });
});
