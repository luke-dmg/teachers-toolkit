import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent} from '@angular/router';
import {PageWithSidebar} from './shared/components/page/page-with-sidebar';
import {Title} from '@angular/platform-browser';
import {MediaMatcher} from '@angular/cdk/layout';
import {Page} from './shared/components/page/page';
import {NavItem} from './shared/models/nav-item';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {

    // state
    isSpinnerVisible = false;
    isSidebarToggleVisible = false;
    mobileQuery: MediaQueryList;
    currentAppTitle: string;
    // routes
    navItems: NavItem[] = [
        {label: 'Kalkulator ocen', path: 'kalkulator-ocen', icon: 'format_list_numbered'},
        {label: 'Średnia ważona', path: 'srednia-wazona', icon: 'school'},
    ];
    // private
    private _currentPageComponent: Page = null;
    private readonly _mobileQueryListener: () => void;

    constructor(
        private titleService: Title,
        private router: Router,
        private route: ActivatedRoute,
        private changeDetectorRef: ChangeDetectorRef,
        private media: MediaMatcher,
    ) {
        // show spinner while lazy loading route
        router.events.subscribe((routerEvent: RouterEvent) => {
            if (
                routerEvent instanceof NavigationStart
            ) {
                this.isSpinnerVisible = true;
            } else if (
                routerEvent instanceof NavigationEnd ||
                routerEvent instanceof NavigationCancel ||
                routerEvent instanceof NavigationError
            ) {
                this.isSpinnerVisible = false;
            }
        });
        // check if is mobile and pass that information to current page component
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => {
            changeDetectorRef.detectChanges();
            if (this._currentPageComponent instanceof Page) {
                this._currentPageComponent.isMobile.next(this.mobileQuery.matches);
            }
        };
        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    onActivate(componentRef) {
        if (componentRef instanceof Page) {
            // store reference to currently displayed page
            this._currentPageComponent = componentRef;
            // set page title in header
            this.currentAppTitle = componentRef.pageName;
            // check if page contains sidebar
            this.isSidebarToggleVisible = componentRef instanceof PageWithSidebar;
            // let the page know if app is viewed on mobile
            this._currentPageComponent.isMobile.next(this.mobileQuery.matches);
        }
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }

    toggleSidebar() {
        if (this._currentPageComponent instanceof PageWithSidebar) {
            this._currentPageComponent.toggleSidebar();
        }
    }
}
